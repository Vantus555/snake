// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerVantusBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeVantusObject.h"
#include "Engine/Classes/Components/InputComponent.h"

// Sets default values
APlayerVantusBase::APlayerVantusBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));

	RootComponent = MainCamera;
}

// Called when the game starts or when spawned
void APlayerVantusBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	CreateSnakeActor();
}

// Called every frame
void APlayerVantusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerVantusBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerVantusBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerVantusBase::HandlePlayerHorizontalInput);
}

void APlayerVantusBase::CreateSnakeActor() {
	Snake = GetWorld()->SpawnActor<ASnakeVantusObject>(SnakeActor, FTransform());
}

void APlayerVantusBase::HandlePlayerVerticalInput(float value) {
	if (IsValid(Snake)) {
		if (value > 0 && Snake->CurrentMove != EMovment::DOWN)
			Snake->CurrentMove = EMovment::UP;
		else if (value < 0 && Snake->CurrentMove != EMovment::UP)
			Snake->CurrentMove = EMovment::DOWN;
	}
}

void APlayerVantusBase::HandlePlayerHorizontalInput(float value) {
	if (IsValid(Snake)) {
		if (value > 0 && Snake->CurrentMove != EMovment::LEFT)
			Snake->CurrentMove = EMovment::RIGHT;
		else if (value < 0 && Snake->CurrentMove != EMovment::RIGHT)
			Snake->CurrentMove = EMovment::LEFT;
	}
}

