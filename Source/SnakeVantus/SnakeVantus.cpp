// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeVantus.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeVantus, "SnakeVantus" );
