// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerVantusBase.generated.h"

class UCameraComponent;
class ASnakeVantusObject;

UCLASS()
class SNAKEVANTUS_API APlayerVantusBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerVantusBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* MainCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeVantusObject* Snake;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeVantusObject> SnakeActor;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
};
