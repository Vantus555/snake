// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleport.h"
#include "SnakeVantusObject.h"
#include "SnakeElementBase.h"

bool ATeleport::Ok = true;

// Sets default values
ATeleport::ATeleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeleport::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleport::Interact(AActor* Interactor, bool bisHead) {
	if (bisHead && Ok) {
		FVector MovmentVector;
		auto Snake = Cast<ASnakeVantusObject>(Interactor);
		if (IsValid(Snake)) {
			this->Ok = false;
			auto location = Snake->SnakeElements[0]->GetActorLocation();
			if(Snake->CurrentMove == EMovment::DOWN || Snake->CurrentMove == EMovment::UP)
				location.X *= -1;
			else location.Y *= -1;
			Snake->SnakeElements[0]->SetActorLocation(location); 
			Snake->Move();
			this->Ok = true;
		}
	}
}

