// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeVantusObject.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovment {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEVANTUS_API ASnakeVantusObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeVantusObject();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeActorElementClass;/**/

	UPROPERTY(EditDefaultsOnly)
		float ElemSize = 100;

	UPROPERTY(EditDefaultsOnly)
		float Speed = 0.5;

	UPROPERTY(EditDefaultsOnly)
		EMovment CurrentMove = EMovment::DOWN;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElemNum = 1);
	UFUNCTION(BlueprintCallable)
		void Move();
	UFUNCTION(BlueprintCallable)
		void NewElement();
	UFUNCTION()
		void overlap(ASnakeElementBase* elem, AActor* other);

};
