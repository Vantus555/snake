// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeVantusObject.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeVantusObject::ASnakeVantusObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeVantusObject::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(Speed);
	AddSnakeElement(4);
}

void ASnakeVantusObject::AddSnakeElement(int ElemNum) {
	for (int i = 0; i < ElemNum; i++) {
		FVector location(SnakeElements.Num() * ElemSize, 0, 0);
		FTransform transform(location);
		ASnakeElementBase* elem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeActorElementClass, transform);
		elem->parent = this;
		int newElem = SnakeElements.Add(elem);

		if (newElem == 0) {
			elem->SetFirstElementType();
		}
	}
}

void ASnakeVantusObject::Move() {
	FVector MovmentVector;
	float MovmentSpeedDelta = ElemSize;
	switch (CurrentMove) {
	case EMovment::UP:
		MovmentVector.X += MovmentSpeedDelta;
		break;
	case EMovment::DOWN:
		MovmentVector.X -= MovmentSpeedDelta;
		break;
	case EMovment::LEFT:
		MovmentVector.Y += MovmentSpeedDelta;
		break;
	case EMovment::RIGHT:
		MovmentVector.Y -= MovmentSpeedDelta;
		break;
	}
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto current = SnakeElements[i];
		auto prev = SnakeElements[i - 1];
		FVector prevLocation = prev->GetActorLocation();
		current->SetActorLocation(prevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovmentVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeVantusObject::NewElement() {
	this->AddSnakeElement();
	this->Move();
}

void ASnakeVantusObject::overlap(ASnakeElementBase* elem, AActor* other) {
	if (IsValid(elem)) {
		int index;
		SnakeElements.Find(elem, index);
		bool b = index == 0;
		IInteractable* inter = Cast<IInteractable>(other);
		if (inter) {
			inter->Interact(this, b);
		}
	}
}

// Called every frame
void ASnakeVantusObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

