// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeVantusObject.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation() {
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HendleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bisHead) {
	auto Snake = Cast<ASnakeVantusObject>(Interactor);
	if (IsValid(Snake)) {
		Snake->Destroy();
	}
}

void ASnakeElementBase::HendleBeginOverlap(
	UPrimitiveComponent* OC, AActor* otherActor,
	UPrimitiveComponent* otherComp,
	int OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult) {

	if (IsValid(parent)) {
		parent->overlap(this, otherActor);
	}
}

void ASnakeElementBase::ToggleCollision() {
	if(MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	else MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

