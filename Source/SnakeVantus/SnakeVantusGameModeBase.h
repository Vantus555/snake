// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeVantusGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEVANTUS_API ASnakeVantusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
